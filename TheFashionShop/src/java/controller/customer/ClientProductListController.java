/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Product;

/**
 *
 * @author nnnn
 */
public class ClientProductListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ClientProductListController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ClientProductListController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String category = request.getParameter("category");
        ProductDAO p = new ProductDAO();
        List<Product> pro = new ArrayList<>();
        if (category == null) {
            category = "";
        }
        switch (category) {
            case "":
                pro = p.getProducts();
                break;
            case "clothes":
                pro = p.getProductsByCateId(1);
                break;
            case "footwear":
                pro = p.getProductsByCateId(2);
                break;
            case "jewelries":
                pro = p.getProductsByCateId(3);
                break;
            case "groceries":
                pro = p.getProductsByCateId(4);
                break;
            default:
                break;
        }

        request.setAttribute("products", pro);
        request.getRequestDispatcher("productList.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        String productName = request.getParameter("productName");
//
//        List<String> cart;
//        if (productName != null && !productName.isEmpty()) {
//            // Lấy session từ request. Nếu không có session, sẽ tạo một session mới
//            HttpSession session = request.getSession(true);
//
//            if (session.getAttribute("cart") == null) {
//                cart = new ArrayList<String>();
//                session.setAttribute("cart", cart);
//            } else {
//                cart = (ArrayList<String>) session.getAttribute("cart");
//            }
//
//            cart.add(productName);
//            session.setAttribute("cart", cart);
//            
//            response.sendRedirect("../cart/list");
//            
//        } else {
//            response.sendRedirect("../product/list");
//        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
