/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dao.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Customer;

/**
 *
 * @author nnnn
 */
public class ViewProfileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProfileController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProfileController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            int cusId = (int) session.getAttribute("cusId");
            
            CustomerDAO cusDAO = new CustomerDAO();
            Customer c = new Customer();
            c = cusDAO.getCustomerByID(cusId);
            
            request.setAttribute("customer", c);
            request.getRequestDispatcher("view.jsp").forward(request, response);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String image = request.getParameter("image");
        String address = request.getParameter("address");
        
        HttpSession session = request.getSession();
        
        Customer customer = new CustomerDAO().getCustomerByID(id);
        Customer newEmail = new CustomerDAO().checkExistedEmail(email);
        Customer newPhone = new CustomerDAO().checkExistedPhone(phone);
        
        if(newEmail != null && !(newEmail.getId() == id)){
            request.setAttribute("customer", customer);
            request.setAttribute("message", "Email is existed");
            request.getRequestDispatcher("view.jsp").forward(request, response);
            return;
        } else if (newPhone != null && !(newPhone.getId() == id)){
            request.setAttribute("customer", customer);
            request.setAttribute("message", "Phone number is existed");
            request.getRequestDispatcher("view.jsp").forward(request, response);
            return;
        }
        CustomerDAO cusDao = new CustomerDAO();
        cusDao.updateUserProfile(id, name, email, phone, image, address);
        request.setAttribute("customer", cusDao.getCustomerByID(id));
        request.setAttribute("message", "Change user profile successfully!");
        session.setAttribute("avatar", image);
        request.getRequestDispatcher("view.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
