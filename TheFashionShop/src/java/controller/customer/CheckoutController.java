/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dao.CartDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Cart;
import model.CartItem;

/**
 *
 * @author nnnn
 */
public class CheckoutController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CheckoutController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CheckoutController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList<CartItem> cart_list = (ArrayList<CartItem>) session.getAttribute("cartList");
        List<Cart> cartProduct = null;
        Object obj = session.getAttribute("cusId");
        int cusId = obj != null ? (int) obj : 0;
        if (cart_list != null) {
            ProductDAO pDao = new ProductDAO();
            cartProduct = pDao.getCartProducts(cart_list);
            request.setAttribute("cusId", cusId);
            request.setAttribute("cartList", cart_list);
            request.setAttribute("cartProducts", cartProduct);
            request.getRequestDispatcher("checkout.jsp").forward(request, response);
        } else {
            request.setAttribute("cusId", cusId);
            request.setAttribute("cartList", cart_list);
            request.setAttribute("cartProducts", cartProduct);
            request.getRequestDispatcher("checkout.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        int finalPrice = Integer.parseInt(request.getParameter("finalPrice"));
//        String name = request.getParameter("name");
//        String address = request.getParameter("address");
//        String phone = request.getParameter("phone");
//        String email = request.getParameter("email");
        CartDAO cart = new CartDAO();
        OrderDAO order = new OrderDAO();
        HttpSession session = request.getSession();
        ArrayList<CartItem> cartList = (ArrayList<CartItem>) session.getAttribute("cartList");

        if (cartList != null) {
            order.createOrder(finalPrice, 1, id);
            int orderId = order.getLatestRecord();
            for (CartItem c : cartList) {
                cart.createCart(c.getQuantity(), c.getId(), id, orderId);
            }

            session.removeAttribute("cartList");
            PrintWriter out = response.getWriter();
            out.println("<script type='text/javascript'>");
            out.println("alert('Pay successfully!');");
            out.println("window.location = '../product/list';");
            out.println("</script>");
        }
//        response.sendRedirect("../product/list");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
