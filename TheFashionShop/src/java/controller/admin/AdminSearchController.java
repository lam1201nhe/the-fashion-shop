/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dao.CategoryDAO;
import dao.CustomerDAO;
import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Customer;
import model.Product;

/**
 *
 * @author nnnn
 */
public class AdminSearchController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminSearchController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminSearchController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cateSearch = request.getParameter("cateSearch");
        String proSearch = request.getParameter("proSearch");
        String accSearch = request.getParameter("accSearch");

        List<Category> listCategory = new ArrayList<>();
        List<Product> listProduct = new ArrayList<>();
        List<Customer> listCustomer = new ArrayList<>();

        CategoryDAO cateDAO = new CategoryDAO();
        ProductDAO proDAO = new ProductDAO();
        CustomerDAO cusDAO = new CustomerDAO();

        if (cateSearch == null) {
            cateSearch = "";
        } else {
            listCategory = cateDAO.searchByName(cateSearch);
            request.setAttribute("listCategory", listCategory);
            request.getRequestDispatcher("categoryList.jsp").forward(request, response);
        }

        if (proSearch == null) {
            proSearch = "";
        } else {
            listProduct = proDAO.searchByName(proSearch);
            request.setAttribute("listProduct", listProduct);
            request.getRequestDispatcher("productList.jsp").forward(request, response);
        }

        if (accSearch == null) {
            accSearch = "";
        } else {
            listCustomer = cusDAO.searchByName(accSearch);
            request.setAttribute("listAccount", listCustomer);
            request.getRequestDispatcher("accountList.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
