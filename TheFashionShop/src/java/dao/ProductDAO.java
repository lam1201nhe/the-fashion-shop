/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Cart;
import model.CartItem;
import model.Category;
import model.Product;

/**
 *
 * @author nnnn
 */
public class ProductDAO {

    Connection conn = null; // dùng de ket noi voi sql sever
    PreparedStatement ps = null;// ném câu lênh quari sang sql sever
    ResultSet rs = null;// dùng de nhan ket qua tra ve

    //GET: all Products
    public List<Product> getProducts() {
        List<Product> list = new ArrayList<>();
        CategoryDAO cateDAO = new CategoryDAO();
        String query = "select * from dbo.Product";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        cateDAO.getCategoryByID(rs.getInt(6)),
                        rs.getString(7),
                        rs.getInt(8)
                ));
            }
        } catch (Exception e) {
            System.out.println("Cannot get all products " + e.getMessage());
        }
        return list;
    }

    //GET: Products by CategoryId
    public List<Product> getProductsByCateId(int categoryID) {
        List<Product> list = new ArrayList<>();
        CategoryDAO cateDAO = new CategoryDAO();
        String query = "select * from Product where CategoryID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, categoryID);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        cateDAO.getCategoryByID(categoryID),
                        rs.getString(7),
                        rs.getInt(8)
                ));
            }
        } catch (Exception e) {
            System.out.println("Cannot get all products by category id" + e.getMessage());
        }
        return list;
    }

    //Tung Lam
    public Product getProductByID(int proID) {
        CategoryDAO cateDAO = new CategoryDAO();
        String query = "select * from dbo.Product where Product.ProductID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, proID);
            rs = ps.executeQuery();
            if (rs.next()) {
                Product p = new Product();
                p.setProductId(rs.getInt(1));
                p.setProductName(rs.getString(2));
                p.setDescription(rs.getString(3));
                p.setPrice(rs.getInt(4));
                p.setStock(rs.getInt(5));
                p.setCategory(cateDAO.getCategoryByID(rs.getInt(6)));
                p.setImage(rs.getString(7));
                p.setStatus(rs.getInt(8));
                return p;
            }
        } catch (Exception e) {
            System.out.println("Cannot get product " + e.getMessage());
        }
        return null;
    }

//   
    public List<Cart> getCartProducts(ArrayList<CartItem> cartList) {
        List<Cart> book = new ArrayList<>();
        try {
            if (cartList.size() > 0) {
                for (CartItem c : cartList) {
                    String query = "select * from dbo.Product where Product.ProductID = ?";
                    conn = DBContext.getConnection();
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, c.getId());
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        Cart row = new Cart();
                        row.setId(rs.getInt("ProductID"));
                        row.setName(rs.getString("ProductName"));
                        row.setPrice(rs.getInt("Price"));
                        row.setQuantity(c.getQuantity());
                        row.setImage(rs.getString("Image"));
                        row.setTotalPrice(rs.getInt("Price") * c.getQuantity());
                        book.add(row);
                    }
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        return book;
    }

    //Tung Lam
    public void updateProduct(int id, String productName, String description, int price, int stock, String image, int CategoryID, int status) {
        String query = "update Product set ProductName = ?"
                + ", Description = ?"
                + ", Price = ?"
                + ", Stock = ? "
                + ", Image = ? "
                + ", CategoryID = ? "
                + ", Status = ? "
                + "where ProductID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, productName);
            ps.setString(2, description);
            ps.setInt(3, price);
            ps.setInt(4, stock);
            ps.setString(5, image);
            ps.setInt(6, CategoryID);
            ps.setInt(7, status);
            ps.setInt(8, id);

            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Cannot get product " + e.getMessage());
        }
    }

    public int getLastProductID() {
        String query = "SELECT TOP 1 ProductID\n"
                + "FROM Product\n"
                + "ORDER BY ProductID DESC";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
                int lastID = rs.getInt(1);
                return lastID;
            }
        } catch (Exception e) {
            System.out.println("Cannot get the last id" + e.getMessage());
        }
        return 0;
    }

    public void createProduct(String productName, String description, int price, int stock, int categoryId, String image) {
        String query = "INSERT INTO Product "
                + "(ProductName, Description, Price, Stock, CategoryID, Image, Status)\n"
                + "VALUES (?, ?, ?, ?, ?, ?, 1);";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, productName);
            ps.setString(2, description);
            ps.setInt(3, price);
            ps.setInt(4, stock);
            ps.setInt(5, categoryId);
            ps.setString(6, image);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteProduct(int productId) {
        String query = "DELETE FROM [dbo].[Product]\n"
                + "      WHERE ProductID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, productId);
            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<Product> searchByName(String name) {
        List<Product> products = new ArrayList<>();
        CategoryDAO cateDAO = new CategoryDAO();
        String query = "select * from Product where ProductName LIKE '%" + name + "%'";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                products.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        cateDAO.getCategoryByID(rs.getInt(6)),
                        rs.getString(7),
                        rs.getInt(8)
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return products;
    }

    public List<Product> filterProducts(String action, String name) {
        List<Product> products = new ArrayList<>();
        CategoryDAO cateDAO = new CategoryDAO();
        String query = "";
        if (action.equals("desc")) {
            query = "SELECT * FROM Product ORDER BY " + name + " DESC";
        } else if (action.equals("inc")) {
            query = "SELECT * FROM Product ORDER BY " + name + " ASC";
        }
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                products.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        cateDAO.getCategoryByID(rs.getInt(6)),
                        rs.getString(7),
                        rs.getInt(8)
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return products;
    }

}
