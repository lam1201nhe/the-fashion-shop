/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Customer;

/**
 *
 * @author Admin
 */
public class CustomerDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public Account checkLogin(String email, String pass) {
        try {
            String query = "select * from Customer where Email =? and Password=?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, pass);
            rs = ps.executeQuery();

            while (rs.next()) {
                Account a = new Account(rs.getString(3), rs.getString(4));
                return a;
            }
        } catch (Exception e) {
        }

        return null;
    }

    public boolean registerUser(String email, String password) {
        try {
            String query = "INSERT INTO Customer (Email, Password) VALUES (?, ?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, password);

            int result = ps.executeUpdate();
            return result > 0;
        } catch (Exception e) {

        } finally {
            // Close resources (Connection, PreparedStatement, ResultSet) here if needed
        }
        return false;
    }

    public Customer getCustomerByID(int id) {
        String query = "select * from Customer where customerID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                Customer cus = new Customer();
                cus.setId(rs.getInt(1));
                cus.setName(rs.getString(2));
                cus.setEmail(rs.getString(3));
                cus.setPassword(rs.getString(4));
                cus.setAddress(rs.getString(5));
                cus.setPhone(rs.getString(6));
                cus.setImage(rs.getString(7));
                cus.setStatus(rs.getInt(8));
                return cus;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Customer checkExistedEmail(String email) {
        try {
            String sql = "SELECT * FROM Customer where Email = ?";
            Connection connection = new DBContext().getConnection();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Customer a = new Customer();
                a.setId(rs.getInt(1));
                a.setName(rs.getString(2));
                a.setPassword(rs.getString(4));
                a.setEmail(rs.getString(3));
                a.setAddress(rs.getString(5));
                a.setPhone(rs.getString(6));
                a.setImage(rs.getString(7));
                a.setStatus(rs.getInt(8));
                return a;
            }
        } catch (SQLException ex) {
            Logger.getLogger(model.Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public List<Customer> getCustomers() {
        List<Customer> list = new ArrayList<>();
        try {
            String sql = "select * from Customer";
            Connection connection = new DBContext().getConnection();
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Customer(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(model.Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public Customer checkExistedPhone(String phone) {
        try {
            String sql = "SELECT * FROM Customer where Phone = ?";
            Connection connection = new DBContext().getConnection();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, phone);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Customer a = new Customer();
                a.setId(rs.getInt(1));
                a.setName(rs.getString(2));
                a.setPassword(rs.getString(4));
                a.setEmail(rs.getString(3));
                a.setAddress(rs.getString(5));
                a.setPhone(rs.getString(6));
                a.setImage(rs.getString(7));
                a.setStatus(rs.getInt(8));
                return a;
            }
        } catch (SQLException ex) {
            Logger.getLogger(model.Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public void updateUserProfile(int id, String name, String email, String phone, String image, String address) {
        try {
            String query = "update Customer set Fullname = ?"
                    + ", Email = ?"
                    + ", Phone = ?"
                    + ", Image = ? "
                    + ", Address = ? "
                    + "where CustomerID = ?";
            Connection connection = new DBContext().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, phone);
            preparedStatement.setString(4, image);
            preparedStatement.setString(5, address);
            preparedStatement.setInt(6, id);
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void changePassword(int id, String newPassword) {
        try {
            String query = "UPDATE Customer set password = ? where CustomerID = ?";
            Connection connection = new DBContext().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, newPassword);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public boolean createCustomer(String fullname, String email, String password, String address, String phone, String image) {
        try {
            String query = "INSERT INTO [dbo].[Customer]\n"
                    + "           ([Fullname]\n"
                    + "           ,[Email]\n"
                    + "           ,[Password]\n"
                    + "           ,[Address]\n"
                    + "           ,[Phone]\n"
                    + "           ,[Image]\n"
                    + "           ,[Status])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,1)";
            Connection connection = new DBContext().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, fullname);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, address);
            preparedStatement.setString(5, phone);
            preparedStatement.setString(6, image);
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
    
    public void deleteAccount(int id) {
        String query = "DELETE FROM [dbo].[Customer]\n"
                + "      WHERE CustomerID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
     public List<Customer> searchByName(String name) {
        List<Customer> list = new ArrayList<>();
        try {
            String sql = "select * from Customer where Fullname Like '%" + name + "%'";
            Connection connection = new DBContext().getConnection();
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Customer(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(model.Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }
}
