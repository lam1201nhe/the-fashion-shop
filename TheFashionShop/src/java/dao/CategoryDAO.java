/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Product;

/**
 *
 * @author nnnn
 */
public class CategoryDAO {

    Connection conn = null; // dùng de ket noi voi sql sever
    PreparedStatement ps = null;// ném câu lênh quari sang sql sever
    ResultSet rs = null;// dùng de nhan ket qua tra ve

    public Category getCategoryByID(int cateID) {
        String query = "select * from dbo.Category where Category.CategoryID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, cateID);
            rs = ps.executeQuery();
            if (rs.next()) {
                Category cate = new Category();
                cate.setId(rs.getInt("CategoryID"));
                cate.setName(rs.getString("Name"));
                cate.setStatus(rs.getInt("Status"));
                return cate;
            }
        } catch (Exception e) {
            System.out.println("Cannot get all products " + e.getMessage());
        }
        return null;
    }

    public List<Product> getProducts() {
        List<Product> list = new ArrayList<>();
        CategoryDAO cateDAO = new CategoryDAO();
        String query = "select * from dbo.Product";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        cateDAO.getCategoryByID(rs.getInt(6)),
                        rs.getString(7),
                        rs.getInt(8)
                ));
            }
        } catch (Exception e) {
            System.out.println("Cannot get all products " + e.getMessage());
        }
        return list;
    }

    public List<Category> getCategories() {
        List<Category> list = new ArrayList<>();
        String query = "select * from dbo.Category";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1),
                        rs.getString(2), rs.getInt(3)
                ));
            }
        } catch (Exception e) {
            System.out.println("Cannot get all products " + e.getMessage());
        }
        return list;
    }

    public Category checkExistedCateName(String name) {
        try {
            String sql = "SELECT * FROM Category where Name=?";
            Connection connection = new DBContext().getConnection();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Category a = new Category();
                a.setId(rs.getInt(1));
                a.setName(rs.getString(2));
                a.setStatus(rs.getInt(3));
                return a;
            }
        } catch (SQLException ex) {
            Logger.getLogger(model.Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public List<Category> searchByName(String name) {
        List<Category> list = new ArrayList<>();
        String query = "select * from Category where Name Like '%" + name + "%'";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1),
                        rs.getString(2), rs.getInt(3)
                ));
            }
        } catch (Exception e) {
            System.out.println("Cannot get all products " + e.getMessage());
        }
        return list;
    }

    public void deleteCategory(int categoryId) {
        String query = "DELETE FROM [dbo].[Category]\n"
                + "      WHERE CategoryID = ?";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, categoryId);
            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void createCategory(String name, int status) {
        String query = "INSERT INTO [dbo].[Category]\n"
                + "           ([Name]\n"
                + "           ,[Status])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";
        try {
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, name);
            ps.setInt(2, status);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Cannot get product " + e.getMessage());
        }
    }

}
