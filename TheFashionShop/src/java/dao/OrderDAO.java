/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import model.Cart;
import model.CartItem;
import model.Order;
import model.OrderD;

/**
 *
 * @author nnnn
 */
public class OrderDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Order> getOrderList(int customerId) {
        List<Order> listOrder = new ArrayList<>();
        try {
            String query = "SELECT Cart.CartID, Product.ProductID, Product.ProductName, Cart.Quantity, Product.Price, Product.Image\n"
                    + "FROM Cart\n"
                    + "INNER JOIN Product ON Cart.ProductId = Product.ProductId\n"
                    + "WHERE Cart.CustomerID = ?;";
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, customerId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order row = new Order();
                row.setCartId(rs.getInt("CartID"));
                row.setProductId(rs.getInt("ProductID"));
                row.setName(rs.getString("ProductName"));
                row.setPrice(rs.getInt("Price"));
                row.setQuantity(rs.getInt("Quantity"));
                row.setImage(rs.getString("Image"));
                row.setTotalPrice(rs.getInt("Price") * rs.getInt("Quantity"));
                listOrder.add(row);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        return listOrder;
    }
    
    public List<Order> getOrderItemByOrderID(int customerId, int orderId) {
        List<Order> listOrder = new ArrayList<>();
        try {
            String query = "SELECT Cart.CartID, Product.ProductID, Product.ProductName, Cart.Quantity, Product.Price, Product.Image\n"
                    + "FROM Cart\n"
                    + "INNER JOIN Product ON Cart.ProductId = Product.ProductId\n"
                    + "WHERE Cart.CustomerID = ? and Cart.OrderID = ?";
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, customerId);
            ps.setInt(2, orderId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order row = new Order();
                row.setCartId(rs.getInt("CartID"));
                row.setProductId(rs.getInt("ProductID"));
                row.setName(rs.getString("ProductName"));
                row.setPrice(rs.getInt("Price"));
                row.setQuantity(rs.getInt("Quantity"));
                row.setImage(rs.getString("Image"));
                row.setTotalPrice(rs.getInt("Price") * rs.getInt("Quantity"));
                listOrder.add(row);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        return listOrder;
    }

    public void createOrder(int totalPrice, int paymentId, int customerId) {
        Date currentDate = new Date(System.currentTimeMillis());
        try {
            String query = "INSERT INTO [dbo].[Order]\n"
                    + "           ([Order_Date]\n"
                    + "           ,[Total_Price]\n"
                    + "           ,[CustomerID]\n"
                    + "           ,[PaymentID])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setDate(1, currentDate);
            ps.setInt(2, totalPrice);
            ps.setInt(3, customerId);
            ps.setInt(4, paymentId);
            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public int getLatestRecord() {
        int latest = 0;
        try {
            String query = "SELECT TOP 1 dbo.[Order].OrderID FROM dbo.[Order] ORDER BY OrderID DESC";
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
                latest = rs.getInt("OrderID");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return latest;
    }

    public List<OrderD> orderHistory(int customerId) {
        List<OrderD> listOrders = new ArrayList<>();
        try {
            String query = "SELECT * FROM dbo.[Order] where CustomerID = ?";
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, customerId);
            rs = ps.executeQuery();

            while (rs.next()) {
                OrderD row = new OrderD();
                row.setOrderId(rs.getInt("OrderID"));
                row.setOrderDate(rs.getDate("Order_Date"));
                row.setTotalPrice(rs.getInt("Total_Price"));
                row.setCustomerId(rs.getInt("CustomerID"));
                listOrders.add(row);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return listOrders;
    }
    
}
