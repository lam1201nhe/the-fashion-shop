/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Cart;
import model.CartItem;

/**
 *
 * @author nnnn
 */
public class CartDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public void createCart(int quantity, int productId, int customerId, int orderID) {
        try {
            String query = "Insert into dbo.Cart\n"
                    + "([Quantity], [CustomerID], [ProductID], [OrderID]) values (?, ?, ?, ?)";
            Connection connection = new DBContext().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, quantity);
            preparedStatement.setInt(2, customerId);
            preparedStatement.setInt(3, productId);
            preparedStatement.setInt(4, orderID);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
