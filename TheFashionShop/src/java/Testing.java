
import dao.CategoryDAO;
import dao.CustomerDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Customer;
import model.Order;
import model.OrderD;
import model.Product;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author nnnn
 */
public class Testing {

    public static void main(String[] args) throws SQLException {
//        ProductDAO proDAO = new ProductDAO();
//        List<Product> listProduct = proDAO.searchProductByName("a");
//        for (Product product : listProduct) {
//            System.out.println(product.toString());
//        }
        
//        CategoryDAO cateDAO = new CategoryDAO();
//        List<Category> listCategory = cateDAO.searchByName("Q");
//        for (Category product : listCategory) {
//            System.out.println(product.toString());
//        }
//        System.out.println("1");
        
        OrderDAO orders = new OrderDAO();
        orders.createOrder(300000, 1, 2);
        List<Order> listOrder =  orders.getOrderItemByOrderID(2, 4);
        for (Order order : listOrder) {
            System.out.println(order.toString());
        }
    }
}
