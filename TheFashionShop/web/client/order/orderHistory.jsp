<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <title>Hexashop - Product Listing Page</title>


        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">

        <link rel="stylesheet" href="../../css/templatemo-hexashop.css">

        <link rel="stylesheet" href="../../css/owl-carousel.css">

        <link rel="stylesheet" href="../../css/lightbox.css">
        <!--
        
        TemplateMo 571 Hexashop
        
        https://templatemo.com/tm-571-hexashop
        
        -->
    </head>

    <body>

        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>  
        <!-- ***** Preloader End ***** -->


        <%@include file="../layout/navbar.jsp" %>

        <c:choose>
            <c:when test="${cusId != 0}">
                <!-- ***** Main Banner Area Start ***** -->
                <div class="page-heading" id="top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="inner-content">
                                    <h2>Order History</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Blank Start -->
                <section class="section" id="products">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">STT</th>
                                                <th scope='col'>Order Date</th>
                                                <th scope="col">Payment</th>
                                                <th scope='col'>Total Price</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:set var="finalPrice" value="0" />
                                            <c:set var="stt" value="0" />
                                            <c:forEach var="item" items="${listOrder}">
                                                <c:set var="stt" value="${stt + 1}" />
                                                <tr>
                                                    <td>${stt}</td>
                                                    <td>
                                                        ${item.orderDate}
                                                    </td>
                                                    <td>
                                                        COD
                                                    </td>
                                                    <td>
                                                        ${item.totalPrice}.000 VN�
                                                    </td>
                                                    <td>
                                                        <a href="./detail?orderId=${item.orderId}">View</a>
                                                    </td>
                                                </tr>
                                                <c:set var="finalPrice" value="${finalPrice + item.totalPrice}"/>
                                            </c:forEach>
                                            <tr style="background-color: #5a6268">
                                                <td> <p style="color: white; font-weight: bold"> TOTAL </p></td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td><p style="font-weight: bold; color:white"> ${finalPrice}.000 VN� </p></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center align-items-center w-100" style="gap: 10px !important">
                                <!--<button class="btn btn-outline-success"><a style="list-style: none; color: inherit;" href="./checkout">Checkout</a></button>-->
                                <button class="btn btn-outline-warning"><a style="list-style: none; color: inherit;" href="../product/list">Continue to shopping</a></button>
                            </div>
                        </div>
                    </div>
                </section>
            </c:when>
            <c:otherwise>
                <section class="section" id="products">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section-heading" style="margin-top: 150px !important">
                                    <h2 style="color: red">You have to login to get your cart and checkout</h2>
                                    <h3>Go to <a href="../../login">LOGIN</a> site</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </c:otherwise>
        </c:choose>



        <%@include file="../layout/footer.jsp" %>


        <!-- jQuery -->
        <script src="../../js/jquery-2.1.0.min.js"></script>

        <!-- Bootstrap -->
        <script src="../../js/popper.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Plugins -->
        <script src="../../js/owl-carousel.js"></script>
        <script src="../../js/accordions.js"></script>
        <script src="../../js/datepicker.js"></script>
        <script src="../../js/scrollreveal.min.js"></script>
        <script src="../../js/waypoints.min.js"></script>
        <script src="../../js/jquery.counterup.min.js"></script>
        <script src="../../js/imgfix.min.js"></script> 
        <script src="../../js/slick.js"></script> 
        <script src="../../js/lightbox.js"></script> 
        <script src="../../js/isotope.js"></script> 

        <!-- Global Init -->
        <script src="../../js/custom.js"></script>

        <script>

            $(function () {
                var selectedClass = "";
                $("p").click(function () {
                    selectedClass = $(this).attr("data-rel");
                    $("#portfolio").fadeTo(50, 0.1);
                    $("#portfolio div").not("." + selectedClass).fadeOut();
                    setTimeout(function () {
                        $("." + selectedClass).fadeIn();
                        $("#portfolio").fadeTo(50, 1);
                    }, 500);

                });
            });

        </script>

    </body>

</html>
