<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <title>Hexashop Ecommerce HTML CSS Template</title>


        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">

        <link rel="stylesheet" href="../../css/templatemo-hexashop.css">

        <link rel="stylesheet" href="../../css/owl-carousel.css">

        <link rel="stylesheet" href="../../css/lightbox.css">
        <!--
        
        TemplateMo 571 Hexashop
        
        https://templatemo.com/tm-571-hexashop
        
        -->
    </head>

    <body>

        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>  
        <!-- ***** Preloader End ***** -->


        <%@include file="../layout/navbar.jsp" %>

        <div class="main-banner" id="top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6"> 
                        <div class="row bg-light rounded align-items-center justify-content-center mx-0 ">
                            <div class="text-center">
                                <form method="post" action="view">
                                    <div class="left-content d-flex flex-column">
                                        <div style="display: none">
                                            <input type="text" name="id" value="${customer.id}"/>
                                        </div>
                                        <div class="mb-3 mt-5">
                                            Full name:
                                            <input type="text" name="name" class="border-input" id="nameInput" value="${customer.name}"/>
                                        </div>

                                        <div class="mb-3">
                                            Email:
                                            <input type="email" name="email" class="border-input" id="emailInput" value="${customer.email}"/>
                                        </div>

                                        <div class="mb-3">
                                            Phone:
                                            <input name="phone" type="text" id="phoneInput" class="border-input" value="${customer.phone}"/>
                                        </div>  
                                        <div class="mb-3">
                                            Address:
                                            <input name="address" type="text" id="addressInput" class="border-input" value="${customer.address}"/>
                                        </div>  
                                        <div class="mb-3">
                                            Image:
                                            <input name="image" type="text" id="imageInput" class="border-input" value="${customer.image}"/>
                                        </div>  
                                        <div class="mb-1s">
                                            <p style="color: red">${requestScope.message}</p>
                                        </div>
                                        <div class="mb-4 mt-4">
                                            <div class="btn btn-danger"><a style="color: white" href="index.jsp">Cancel</a></div>
                                            <button class="btn btn-primary" type="submit">Save Changes</button>
                                            <div class="btn btn-dark"><a style="color: white !important" href="./changePassword">Change Password</a></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--                        <div class="col-md-5">
                                                        <form method="post" action="UploadImage" id="frmUploadImage"
                                                              enctype="multipart/form-data">
                                                            <input type="file" name="avatar_img" id="avatarImage" onchange="handleFileChange()"/>
                                                        </form>
                            
                                                    </div>-->
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div>
                            <img style="width: 50%; object-fit: cover" src="${customer.image != null ? customer.image : 'https://phongreviews.com/wp-content/uploads/2022/11/avatar-facebook-mac-dinh-19.jpg'}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="../layout/footer.jsp" %>


        <!-- jQuery -->
        <script src="../../js/jquery-2.1.0.min.js"></script>

        <!-- Bootstrap -->
        <script src="../../js/popper.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Plugins -->
        <script src="../../js/owl-carousel.js"></script>
        <script src="../../js/accordions.js"></script>
        <script src="../../js/datepicker.js"></script>
        <script src="../../js/scrollreveal.min.js"></script>
        <script src="../../js/waypoints.min.js"></script>
        <script src="../../js/jquery.counterup.min.js"></script>
        <script src="../../js/imgfix.min.js"></script> 
        <script src="../../js/slick.js"></script> 
        <script src="../../js/lightbox.js"></script> 
        <script src="../../js/isotope.js"></script> 

        <!-- Global Init -->
        <script src="../../js/custom.js"></script>

        <script>

            $(function () {
                var selectedClass = "";
                $("p").click(function () {
                    selectedClass = $(this).attr("data-rel");
                    $("#portfolio").fadeTo(50, 0.1);
                    $("#portfolio div").not("." + selectedClass).fadeOut();
                    setTimeout(function () {
                        $("." + selectedClass).fadeIn();
                        $("#portfolio").fadeTo(50, 1);
                    }, 500);

                });
            });

        </script>

    </body>
</html>
