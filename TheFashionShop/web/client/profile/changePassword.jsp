<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <title>Hexashop Ecommerce HTML CSS Template</title>


        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">

        <link rel="stylesheet" href="../../css/templatemo-hexashop.css">

        <link rel="stylesheet" href="../../css/owl-carousel.css">

        <link rel="stylesheet" href="../../css/lightbox.css">
        <!--
        
        TemplateMo 571 Hexashop
        
        https://templatemo.com/tm-571-hexashop
        
        -->
    </head>

    <body>

        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>  
        <!-- ***** Preloader End ***** -->


        <%@include file="../layout/navbar.jsp" %>

        <div class="main-banner" id="top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6"> 
                        <div class="row bg-light rounded align-items-center justify-content-center mx-0 ">
                            <div class="text-center">
                                <form method="post" action="changePassword">

                                    <div class="left-content d-flex flex-column">
                                        <!-- Get the user_id through Session -->
                                        <div style="display: none">
                                            <input type="text" name="id" value="${customer.id}"/>
                                        </div>

                                        <div class="mb-3 mt-5">
                                            Old Password:
                                            <input type="password" name="oldPassword" class="border-input" id="oldPassword"/>
                                        </div>
                                        <!--                                    <div>
                                                                                <p style="font-size: 15px"><em>Password must have at least 8 characters including 1 uppercase letter, 1 number and 1 special character</em></p>
                                                                            </div>-->
                                        <div class="mb-3">
                                            New Password:
                                            <input type="password" name="newPassword" class="border-input" id="newPassword"/>
                                        </div>
                                        <div class="mb-3">
                                            Confirm Password
                                            <input type="password" id="confirmPassword" class="border-input" name="confirmPassword"/>
                                        </div>
                                        <div class="mb-1">
                                            <p style="color: red">${requestScope.message}</p>
                                        </div>
                                    </div>  
                                    <div class="mb-4 mt-4">
                                        <div class="btn btn-danger"><a href="view" style="color: white">Cancel</a></div>
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </div>
                                </form>
                            </div>
                            <!--                        <div class="col-md-5">
                                                        <form method="post" action="UploadImage" id="frmUploadImage"
                                                              enctype="multipart/form-data">
                                                            <input type="file" name="avatar_img" id="avatarImage" onchange="handleFileChange()"/>
                                                        </form>
                            
                                                    </div>-->
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div>
                            <img style="width: 50%; object-fit: cover" src="${customer.image != null ? customer.image : 'https://phongreviews.com/wp-content/uploads/2022/11/avatar-facebook-mac-dinh-19.jpg'}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="../layout/footer.jsp" %>


        <!-- jQuery -->
        <script src="../../js/jquery-2.1.0.min.js"></script>

        <!-- Bootstrap -->
        <script src="../../js/popper.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Plugins -->
        <script src="../../js/owl-carousel.js"></script>
        <script src="../../js/accordions.js"></script>
        <script src="../../js/datepicker.js"></script>
        <script src="../../js/scrollreveal.min.js"></script>
        <script src="../../js/waypoints.min.js"></script>
        <script src="../../js/jquery.counterup.min.js"></script>
        <script src="../../js/imgfix.min.js"></script> 
        <script src="../../js/slick.js"></script> 
        <script src="../../js/lightbox.js"></script> 
        <script src="../../js/isotope.js"></script> 

        <!-- Global Init -->
        <script src="../../js/custom.js"></script>

        <script>

            $(function () {
                var selectedClass = "";
                $("p").click(function () {
                    selectedClass = $(this).attr("data-rel");
                    $("#portfolio").fadeTo(50, 0.1);
                    $("#portfolio div").not("." + selectedClass).fadeOut();
                    setTimeout(function () {
                        $("." + selectedClass).fadeIn();
                        $("#portfolio").fadeTo(50, 1);
                    }, 500);

                });
            });

        </script>

    </body>
</html>
