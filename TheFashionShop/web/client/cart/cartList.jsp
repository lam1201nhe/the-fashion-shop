<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <title>Hexashop - Product Listing Page</title>


        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">

        <link rel="stylesheet" href="../../css/templatemo-hexashop.css">

        <link rel="stylesheet" href="../../css/owl-carousel.css">

        <link rel="stylesheet" href="../../css/lightbox.css">
        <!--
        
        TemplateMo 571 Hexashop
        
        https://templatemo.com/tm-571-hexashop
        
        -->
    </head>

    <body>

        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>  
        <!-- ***** Preloader End ***** -->


        <%@include file="../layout/navbar.jsp" %>

        <c:choose>
            <c:when test="${cusId != 0}">
                <!-- ***** Main Banner Area Start ***** -->
                <div class="page-heading" id="top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="inner-content">
                                    <h2>My Cart</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Blank Start -->
                <section class="section" id="products">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope='col'>Quantity</th>
                                                <th scope='col'>Price</th>
                                                <th scope='col'>Total Price</th>
                                                <th scope="col">Delete Item</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:if test="${requestScope.cartList != null}"> 
                                                <c:set var="finalPrice" value="0" />
                                                <c:forEach var="item" items="${requestScope.cartProducts}">
                                                    <tr>
                                                        <td>${item.name}</td>
                                                        <td>
                                                            <form method="post">
                                                                <!--                                                            <div class="quantity buttons_added">-->
                                                                <!--<input />-->
                                                                <div class="form-group d-flex justify-content-between">
                                                                    <button><a class="btn bnt-sm btn-incre" href="quantity?action=dec&id=${item.id}"> -  </a> </button>
                                                                    <input type="text" name="quantity" class="form-control"  value="${item.quantity}" readonly> 
                                                                    <button><a class="btn btn-sm btn-decre" href="quantity?action=inc&id=${item.id}"> + </a></button>
                                                                </div>
                                                            </form>
                                                            <!--<input type="button" value="-" class="minus"><input type="number" step="1" min="1" max="" name="quantity" value="${item.quantity}" title="Qty" class="input-text qty text" size="4" pattern="" inputmode=""><input type="button" value="+" class="plus">-->
                                                            <!--</div>-->
                                                        </td>
                                                        <td>
                                                            ${item.price}.000 VN�
                                                        </td>
                                                        <td>
                                                            ${item.totalPrice}.000 VN�
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-danger"><a href="deleteCartItem?id=${item.id}" style="color: inherit">X</a></button>
                                                        </td>
                                                    </tr>
                                                    <c:set var="finalPrice" value="${finalPrice + item.totalPrice}"/>
                                                </c:forEach>
                                                <tr style="background-color: #5a6268">
                                                    <td> <p style="color: white; font-weight: bold"> TOTAL </p></td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <p style="font-weight: bold; color:white"> ${finalPrice}.000 VN� </p>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </c:if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center align-items-center w-100" style="gap: 10px !important">
                                <button class="btn btn-outline-success"><a style="list-style: none; color: inherit;" href="./checkout">Checkout</a></button>
                                <button class="btn btn-outline-warning"><a style="list-style: none; color: inherit;" href="../product/list">Continue to shopping</a></button>
                            </div>
                        </div>
                    </div>
                </section>
            </c:when>
            <c:otherwise>
                <section class="section" id="products">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section-heading" style="margin-top: 150px !important">
                                    <h2 style="color: red">You have to login to get your cart and checkout</h2>
                                    <h3>Go to <a href="../../login">LOGIN</a> site</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </c:otherwise>
        </c:choose>



        <%@include file="../layout/footer.jsp" %>


        <!-- jQuery -->
        <script src="../../js/jquery-2.1.0.min.js"></script>

        <!-- Bootstrap -->
        <script src="../../js/popper.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Plugins -->
        <script src="../../js/owl-carousel.js"></script>
        <script src="../../js/accordions.js"></script>
        <script src="../../js/datepicker.js"></script>
        <script src="../../js/scrollreveal.min.js"></script>
        <script src="../../js/waypoints.min.js"></script>
        <script src="../../js/jquery.counterup.min.js"></script>
        <script src="../../js/imgfix.min.js"></script> 
        <script src="../../js/slick.js"></script> 
        <script src="../../js/lightbox.js"></script> 
        <script src="../../js/isotope.js"></script> 

        <!-- Global Init -->
        <script src="../../js/custom.js"></script>

        <script>

            $(function () {
                var selectedClass = "";
                $("p").click(function () {
                    selectedClass = $(this).attr("data-rel");
                    $("#portfolio").fadeTo(50, 0.1);
                    $("#portfolio div").not("." + selectedClass).fadeOut();
                    setTimeout(function () {
                        $("." + selectedClass).fadeIn();
                        $("#portfolio").fadeTo(50, 1);
                    }, 500);

                });
            });

        </script>

    </body>

</html>
