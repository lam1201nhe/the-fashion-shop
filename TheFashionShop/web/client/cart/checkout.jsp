<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <title>Hexashop - Product Detail Page</title>


        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">

        <link rel="stylesheet" href="../../css/templatemo-hexashop.css">

        <link rel="stylesheet" href="../../css/owl-carousel.css">

        <link rel="stylesheet" href="../../css/lightbox.css">
        <!--
        
        TemplateMo 571 Hexashop
        
        https://templatemo.com/tm-571-hexashop
        
        -->
    </head>

    <body>

        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>  
        <!-- ***** Preloader End ***** -->


        <%@include file="../layout/navbar.jsp" %>

        <!-- ***** Main Banner Area Start ***** -->
        <div class="page-heading" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-content">
                            <h2>Checkout Information</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- comment -->

        <!-- ***** Product Area Starts ***** -->
        <section class="section" id="product">
            <div class="container">

                <div class="row">
                    <div class="col-lg-7">

                        <div style="border: 1px dashed gray; padding: 20px;">
                            <h2 class="fw-bold mb-2 dangnhap text-uppercase">Th�ng tin c� nh�n</h2>
                            
                            <div class="mb-1">
                                Full Name <br>
                                <input
                                    type="text"
                                    value="${sessionScope.account.name}"
                                    name="name"
                                    style="width: 100%"
                                    />
                            </div>

                            <div class="mb-1">
                                Email <br>
                                <input
                                    type="text"
                                    value="${sessionScope.account.email}"
                                    name="email"
                                    style="width: 100%"
                                    />
                            </div>

                            <div class="mb-1">
                                Address <br>
                                <input
                                    type="text"
                                    value="${sessionScope.account.address}"
                                    name="address"
                                    style="width: 100%"
                                    />
                            </div>

                            <div class="mb-1">
                                Phone <br>
                                <input
                                    type="text"
                                    value="${sessionScope.account.phone}"
                                    name="phone"
                                    style="width: 100%"
                                    />
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-5">
                        <div class="right-content">
                            <div style="border: 1px solid green; padding: 15px;">
                                <form action='checkout' method="post">
                                    <input type='hidden' value='${sessionScope.account.id}' name="id"/>
                                    <h2>Cart Item</h2>
                                    <div class="row">
                                        <c:choose>
                                            <c:when test="${requestScope.cartList != null}">
                                                <c:set var="finalPrice" value="0"/>
                                                <c:set var="totalQuantity" value="0"/>
                                                <c:forEach var="item" items="${requestScope.cartProducts}">
                                                    <div class="col-lg-5" style='font-style: italic; font-size: 14px'>
                                                        ${item.name} x ${item.quantity}
                                                    </div>
                                                    <div class="col-lg-3">
                                                        =
                                                    </div>
                                                    <div class="col-lg-4">
                                                        ${item.totalPrice}.000 VN�
                                                    </div>
                                                    <c:set var="totalQuantity" value="${totalQuantity + item.quantity}"/>
                                                    <c:set var="finalPrice" value="${finalPrice + item.totalPrice}"/>
                                                </c:forEach>
                                                <hr style='border-color: black'/>
                                                <div class="col-lg-5">
                                                    TOTAL
                                                </div>
                                                <div class="col-lg-3">
                                                    =
                                                </div>
                                                <div class="col-lg-4" style='font-weight: bold'>
                                                    ${finalPrice}.000 VN�
                                                </div>

                                            </c:when>
                                            <c:otherwise>
                                                <div class="col-lg-12">
                                                    Your cart have nothing to display!!
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <input type="hidden" value="${finalPrice}" name="finalPrice"/>
                                        <c:choose>
                                            <c:when test="${requestScope.cartList != null}">
                                                <button class='btn btn-outline-success' type='submit'>Finish the payment</button>
                                            </c:when>
                                            <c:otherwise>
                                                <button class='btn btn-outline-success' type='button' disabled="">Finish the payment</button>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Product Area Ends ***** -->

        <%@include file="../layout/footer.jsp" %>


        <!-- jQuery -->
        <script src="../../js/jquery-2.1.0.min.js"></script>

        <!-- Bootstrap -->
        <script src="../../js/popper.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Plugins -->
        <script src="../../js/owl-carousel.js"></script>
        <script src="../../js/accordions.js"></script>
        <script src="../../js/datepicker.js"></script>
        <script src="../../js/scrollreveal.min.js"></script>
        <script src="../../js/waypoints.min.js"></script>
        <script src="../../js/jquery.counterup.min.js"></script>
        <script src="../../js/imgfix.min.js"></script> 
        <script src="../../js/slick.js"></script> 
        <script src="../../js/lightbox.js"></script> 
        <script src="../../js/isotope.js"></script> 
        <script src="../../js/quantity.js"></script>

        <!-- Global Init -->
        <script src="../../js/custom.js"></script>

        <script>

            $(function () {
                var selectedClass = "";
                $("p").click(function () {
                    selectedClass = $(this).attr("data-rel");
                    $("#portfolio").fadeTo(50, 0.1);
                    $("#portfolio div").not("." + selectedClass).fadeOut();
                    setTimeout(function () {
                        $("." + selectedClass).fadeIn();
                        $("#portfolio").fadeTo(50, 1);
                    }, 500);

                });
            });

        </script>

    </body>

</html>
