<%-- 
    Document   : navbar
    Created on : Jan 23, 2024, 10:32:38 PM
    Author     : nnnn
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="index.html" class="logo">
                        <img src="../../images/logo.png">
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li class="scroll-to-section"><a href="/TheFashionShop/" class="active">Home</a></li>
                        <li class="scroll-to-section"><a href="/TheFashionShop/client/product/list">Products</a></li>
                        <li class="submenu">
                            <a href="javascript:;">Categories</a>
                            <ul>
                                <li><a href="/TheFashionShop/client/product/list?category=clothes">Clothes</a></li>
                                <li><a href="/TheFashionShop/client/product/list?category=footwear">Footwear</a></li>
                                <li><a href="/TheFashionShop/client/product/list?category=jewelries">Jewelries</a></li>
                                <li><a href="/TheFashionShop/client/product/list?category=groceries">Groceries</a></li>
                            </ul>
                        </li>
                        <li class="submenu">
                            <c:choose>
                                <c:when test="${sessionScope.cusId != null}">
                                    <a href="javascript:;"><img style="width: 50px;
                                                                height: 50px;
                                                                object-fit: cover;
                                                                border-radius: 50%" src="${sessionScope.avatar}"/></a>
                                    <ul>
                                        <li><a href="/TheFashionShop/client/profile/view">Profile</a></li>
                                        <li><a href="/TheFashionShop/client/order/history">Order History</a></li>
                                        <li><a href="/TheFashionShop/client/order/list">All purchased item</a></li>
                                        <li><a href="/TheFashionShop/client/cart/list">My Cart (${sessionScope.cartList.size() > 0 ? sessionScope.cartList.size() : 0})</a></li>
                                        <li>
                                            <form action="logout">
                                                <a href="/TheFashionShop/logout">Logout</a>
                                            </form>
                                        </li>
                                    </ul>
                                </c:when>
                                <c:otherwise>
                                    <a href="javascript:;">Login/Register</a>
                                    <ul>
                                        <li><a href="/TheFashionShop/login">Login</a></li>
                                        <li><a href="/TheFashionShop/register">Register</a></li>
                                    </ul>
                                </c:otherwise>
                            </c:choose>
                        </li>
                    </ul>        
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->
