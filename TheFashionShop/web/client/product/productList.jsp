<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <title>Hexashop - Product Listing Page</title>


        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">

        <link rel="stylesheet" href="../../css/templatemo-hexashop.css">

        <link rel="stylesheet" href="../../css/owl-carousel.css">

        <link rel="stylesheet" href="../../css/lightbox.css">
        <!--
        
        TemplateMo 571 Hexashop
        
        https://templatemo.com/tm-571-hexashop
        
        -->
    </head>

    <body>

        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>  
        <!-- ***** Preloader End ***** -->


        <%@include file="../layout/navbar.jsp" %>
        <!-- ***** Main Banner Area Start ***** -->
        <div class="page-heading" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-content">
                            <h2>Check Our Products</h2>
                            <span>Awesome &amp; Creative HTML CSS layout by TemplateMo</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Main Banner Area End ***** -->


        <!-- ***** Products Area Starts ***** -->
        <section class="section" id="products">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-heading">
                            <h2>Our Latest Products</h2>
                            <span>Check out all of our products.</span>
                        </div>

                        <div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 ">
                        <form action="search">
                            <div class="d-flex justify-content-between mb-1 w-100">
                                <input id="searchInput" style='width: 90%; padding: 12px' name='searchQuery' placeholder="Search for product's name..." />
                                <button class='btn btn-primary' type='submit'>Search</button>
                            </div>
                        </form>
                        <div class="dropdown mb-3">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Filter Products
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="./filter?action=desc&name=Price">Desc: Price</a>
                                <a class="dropdown-item" href="./filter?action=inc&name=Price">Asc: Price</a>
                                <a class="dropdown-item" href="./filter?action=desc&name=ProductName">Desc Title</a>
                                <a class="dropdown-item" href="./filter?action=inc&name=ProductName">Asc: Title</a>
                            </div>
                        </div>

                    </div>
                    <c:forEach items="${products}" var="p">
                        <c:if test="${p.status == 1}">
                            <div class="col-lg-4">
                                <div class="item">
                                    <div class="thumb">
                                        <div class="hover-content">
                                            <ul class='d-flex justify-content-center'>
                                                <li><a href="./detail?proID=${p.productId}"><i class="fa fa-eye"></i></a></li>
                                                <li>
                                                    <a href="addToCart?id=${p.productId}"><i class="fa fa-shopping-cart"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <img src="${p.image != "" ? p.image : 'https://phongreviews.com/wp-content/uploads/2022/11/avatar-facebook-mac-dinh-19.jpg'}" alt="">
                                    </div>
                                    <div class="down-content">
                                        <h4>${p.productName}</h4>
                                        <span>${p.price}.000 VN�</span>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>

                    <div class="col-lg-12">
                        <div class="pagination">
                            <ul>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#">3</a>
                                </li>
                                <li>
                                    <a href="#">4</a>
                                </li>
                                <li>
                                    <a href="#">></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Products Area Ends ***** -->

        <%@include file="../layout/footer.jsp" %>


        <!-- jQuery -->
        <script src="../../js/jquery-2.1.0.min.js"></script>

        <!-- Bootstrap -->
        <script src="../../js/popper.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Plugins -->
        <script src="../../js/owl-carousel.js"></script>
        <script src="../../js/accordions.js"></script>
        <script src="../../js/datepicker.js"></script>
        <script src="../../js/scrollreveal.min.js"></script>
        <script src="../../js/waypoints.min.js"></script>
        <script src="../../js/jquery.counterup.min.js"></script>
        <script src="../../js/imgfix.min.js"></script> 
        <script src="../../js/slick.js"></script> 
        <script src="../../js/lightbox.js"></script> 
        <script src="../../js/isotope.js"></script> 

        <!-- Global Init -->
        <script src="../../js/custom.js"></script>

        <script>
            $(function () {
                var selectedClass = "";
                $("p").click(function () {
                    selectedClass = $(this).attr("data-rel");
                    $("#portfolio").fadeTo(50, 0.1);
                    $("#portfolio div").not("." + selectedClass).fadeOut();
                    setTimeout(function () {
                        $("." + selectedClass).fadeIn();
                        $("#portfolio").fadeTo(50, 1);
                    }, 500);

                });
            });

        </script>

    </body>

</html>
