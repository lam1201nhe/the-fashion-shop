<%-- Document : addSubject Created on : Oct 7, 2023, 10:58:01 PM Author : Admin --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>DASHMIN - Bootstrap Admin Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="keywords" />
        <meta content="" name="description" />

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon" />

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
            href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600;700&display=swap"
            rel="stylesheet"
            />

        <!-- Icon Font Stylesheet -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css"
            rel="stylesheet"
            />
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css"
            rel="stylesheet"
            />

        <!-- Libraries Stylesheet -->
        <link
            href="../lib/owlcarousel/assets/owl.carousel.min.css"
            rel="stylesheet"
            />
        <link
            href="../lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css"
            rel="stylesheet"
            />

        <!-- Customized Bootstrap Stylesheet -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" />

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet" />
    </head>

    <body>
        <div class="container-xxl position-relative bg-white d-flex p-0">
            <%@include file="../layout/spinner.jsp" %>
            <%@include file="../layout/sidebar.jsp" %>
            <!-- Content Start -->
            <div class="content">
                <%@include file="../layout/navbar.jsp" %>
                <!-- Blank Start -->
                <div class="container-fluid pt-4 px-4">
                    <div class="col-xl-12">
                        <div class="bg-light rounded h-100 p-4">
                            <form method="post" action="create">
                                <div>
                                    <h2 class="mb-4">Create Category</h2>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-text" id="basic-addon1">ID</span>
                                    <input
                                        type="text"
                                        class="form-control"
                                        readonly
                                        name="id"
                                        aria-describedby="idhelp"
                                        />
                                </div>
                                <div id="idhelp" class="form-text mb-3">
                                    ID will increase automatically.
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon2"
                                          >Category Name</span
                                    >
                                    <input
                                        type="text"
                                        name="name"
                                        class="form-control"
                                        />

                                </div>
                                <div class="input-group mb-3">
                                    <input
                                        type="radio"
                                        class="btn-check"
                                        name="btnradio"
                                        value="active"
                                        autocomplete="off"
                                        checked
                                        />
                                    <label class="btn btn-outline-success" for="btnradio1"
                                           >Active</label
                                    >
                                    <input
                                        type="radio"
                                        class="btn-check"
                                        name="btnradio"
                                        value="deactive"
                                        autocomplete="off"
                                        />
                                    <label class="btn btn-outline-danger" for="btnradio2"
                                           >Deactive</label
                                    >
                                </div>
                                <hr />
                                <div class="mb-2 d-flex justify-content-center" style="gap: 10px">
                                    <button class="btn btn-primary" type="submit">
                                        Add category
                                    </button>

                                    <div class="btn btn-danger">
                                        <a style="color: white" href="../category/list">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <%@include file="../layout/footer.jsp" %>
            </div>
            <!-- Content End -->

            <!-- Back to Top -->
            <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"
               ><i class="bi bi-arrow-up"></i
                ></a>
        </div>

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="../lib/chart/chart.min.js"></script>
        <script src="../lib/easing/easing.min.js"></script>
        <script src="../lib/waypoints/waypoints.min.js"></script>
        <script src="../lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="../lib/tempusdominus/js/moment.min.js"></script>
        <script src="../lib/tempusdominus/js/moment-timezone.min.js"></script>
        <script src="../lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>
        <script
            src="https://kit.fontawesome.com/1d015607ed.js"
            crossorigin="anonymous"
        ></script>
        <!-- Template Javascript -->
        <script src="../js/main.js"></script>
    </body>
</html>
