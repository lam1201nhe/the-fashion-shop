<%-- 
    Document   : sidebar
    Created on : Oct 6, 2023, 11:46:12 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <!-- Sidebar Start -->
        <div class="sidebar pe-4 pb-3">
            <nav class="navbar bg-light navbar-light">
                <a href="index.html" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary"><i class="fa fa-hashtag me-2"></i>DASHMIN</h3>
                </a>
                <div class="d-flex align-items-center ms-4 mb-4">
                    <div class="position-relative">
                        <img class="rounded-circle" src="../img/user.jpg" alt="" style="width: 40px; height: 40px;">
                        <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                    </div>
                    <div class="ms-3">
                        <h6 class="mb-0">Jhon Doe</h6>
                        <span>Admin</span>
                    </div>
                </div>
                <div class="navbar-nav w-100">
                    <a href="../product/list" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Products</a>
                    <a href="../category/list" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Categories</a>
                    <a href="../account/list" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Accounts</a>
                </div>
            </nav>
        </div>
        <!-- Sidebar End -->