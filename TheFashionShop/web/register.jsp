<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <title>Hexashop - Product Detail Page</title>


        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">

        <link rel="stylesheet" href="css/templatemo-hexashop.css">

        <link rel="stylesheet" href="css/owl-carousel.css">

        <link rel="stylesheet" href="css/lightbox.css">
        <!-- Font Awesome -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.css"
            rel="stylesheet"
            />

    </head>
    <body>
        <div>
            <!-- ***** Header Area Start ***** -->
            <header class="header-area header-sticky">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="main-nav">
                                <!-- ***** Logo Start ***** -->
                                <a href="index.html" class="logo">
                                    <img src="images/logo.png">
                                </a>
                                <!-- ***** Logo End ***** -->
                                <!-- ***** Menu Start ***** -->
                                <ul class="nav">
                                    <li class="scroll-to-section"><a href="/TheFashionShop/" class="active">Home</a></li>
                                    <li class="scroll-to-section"><a href="/TheFashionShop/client/product/list?category=">Products</a></li>
                                    <li class="submenu">
                                        <a href="javascript:;">Categories</a>
                                        <ul>
                                            <li><a href="/TheFashionShop/client/product/list?category=clothes">Clothes</a></li>
                                            <li><a href="/TheFashionShop/client/product/list?category=footwear">Footwear</a></li>
                                            <li><a href="/TheFashionShop/client/product/list?category=jewelries">Jewelries</a></li>
                                            <li><a href="/TheFashionShop/client/product/list?category=groceries">Groceries</a></li>
                                        </ul>
                                    </li>
                                    <li class="submenu">
                                        <a href="javascript:;">Login/Register</a>
                                        <ul>
                                            <li><a href="/TheFashionShop/login">Login</a></li>
                                            <li><a href="/TheFashionShop/register">Register</a></li>
                                        </ul>
                                    </li>
                                </ul>        
                                <a class='menu-trigger'>
                                    <span>Menu</span>
                                </a>
                                <!-- ***** Menu End ***** -->
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <!-- ***** Header Area End ***** -->

            <!-- ***** Main Content ***** -->
            <section class="vh-100 pt-5 gradient-custom mt-5 mb-5">
                <div class="container py-5 h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                            <div class="card" style="border-radius: 1rem">
                                <div style="border: 1px solid gray; border-radius: 20px;" class="card-body p-5 text-center">
                                    <div class="mb-md-5 mt-md-4 pb-5">
                                        <form action="register" method="POST">
                                            <h2 class="fw-bold mb-2 dangnhap text-uppercase">Đăng ký</h2>
                                            <p class="text-50 dangnhap">
                                                Vui lòng nhập thông tin tài khoản
                                            </p>
                                            <div >
                                                <p style="color: red">${requestScope.message}</p>
                                            </div>
                                            <div class="form-outline mb-4">
                                                <input
                                                    type="text"
                                                    id="typeEmailX"
                                                    class="form-control form-control-lg"
                                                    name="fullname"
                                                    />
                                                <label class="form-label dangnhap" for="typeEmailX"
                                                       >Họ và tên*</label
                                                >
                                            </div>

                                            <div class="form-outline mb-4">
                                                <input
                                                    type="text"
                                                    id="typeEmailX"
                                                    class="form-control form-control-lg"
                                                    name="email"
                                                    />
                                                <label class="form-label dangnhap" for="typeEmailX"
                                                       >Email*</label
                                                >
                                            </div>

                                            <div class="form-outline mb-4">
                                                <input
                                                    type="password"
                                                    id="typePasswordX"
                                                    class="form-control form-control-lg"
                                                    name="password"
                                                    />
                                                <label class="form-label dangnhap" for="typePasswordX"
                                                       >Mật khẩu*</label
                                                >
                                            </div>

                                            <div class="form-outline mb-4">
                                                <input
                                                    type="text"
                                                    id="typePasswordX"
                                                    class="form-control form-control-lg"
                                                    name="image"
                                                    />
                                                <label class="form-label dangnhap" for="typePasswordX"
                                                       >Ảnh đại diện*</label
                                                >
                                            </div>

                                            <div class="form-outline mb-4">
                                                <input
                                                    type="text"
                                                    id="typePasswordX"
                                                    class="form-control form-control-lg"
                                                    name="address"
                                                    />
                                                <label class="form-label dangnhap" for="typePasswordX"
                                                       >Địa chỉ*</label
                                                >
                                            </div>

                                            <div class="form-outline mb-4">
                                                <input
                                                    type="text"
                                                    id="typePasswordX"
                                                    class="form-control form-control-lg"
                                                    name="phone"
                                                    />
                                                <label class="form-label dangnhap" for="typePasswordX"
                                                       >Điện thoại*</label
                                                >
                                            </div>

                                            <button
                                                class="btn btn-outline-success btn-lg px-5 btn-login"
                                                type="submit"
                                                id="btn-login"
                                                >
                                                Register
                                            </button>

                                        </form>
                                        <p class='mt-2'> Do u have an account? <a href='./login'> Login </a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <footer class='mt-5' style="margin-top: 350px !important">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="first-item">
                                <div class="logo">
                                    <img src="images/white-logo.png" alt="hexashop ecommerce templatemo">
                                </div>
                                <ul>
                                    <li><a href="#">16501 Collins Ave, Sunny Isles Beach, FL 33160, United States</a></li>
                                    <li><a href="#">hexashop@company.com</a></li>
                                    <li><a href="#">010-020-0340</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <h4>Shopping &amp; Categories</h4>
                            <ul>
                                <li><a href="#">Men’s Shopping</a></li>
                                <li><a href="#">Women’s Shopping</a></li>
                                <li><a href="#">Kid's Shopping</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <h4>Useful Links</h4>
                            <ul>
                                <li><a href="#">Homepage</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Help</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <h4>Help &amp; Information</h4>
                            <ul>
                                <li><a href="#">Help</a></li>
                                <li><a href="#">FAQ's</a></li>
                                <li><a href="#">Shipping</a></li>
                                <li><a href="#">Tracking ID</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-12">
                            <div class="under-footer">
                                <p>Copyright © 2022 HexaShop Co., Ltd. All Rights Reserved. 

                                    <br>Design: <a href="https://templatemo.com" target="_parent" title="free css templates">TemplateMo</a></p>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
    <!-- MDB -->
    <script
        type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.js"
    ></script>
    <!-- jQuery -->
    <script src="js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="js/owl-carousel.js"></script>
    <script src="js/accordions.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/scrollreveal.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imgfix.min.js"></script> 
    <script src="js/slick.js"></script> 
    <script src="js/lightbox.js"></script> 
    <script src="js/isotope.js"></script> 

    <!-- Global Init -->
    <script src="js/custom.js"></script>

    <script>

        $(function () {
            var selectedClass = "";
            $("p").click(function () {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut();
                setTimeout(function () {
                    $("." + selectedClass).fadeIn();
                    $("#portfolio").fadeTo(50, 1);
                }, 500);

            });
        });

    </script>
</html>
